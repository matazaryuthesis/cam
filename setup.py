from setuptools import setup

setup(
    name='CAM',
    version='',
    packages=['cam', 'cam.gui', 'cam.gui.colors', 'cam.gui.renderer', 'cam.rule', 'cam.lattice', 'cam.parsers'],
    install_requires=[
        'PyQt5',
        'numpy',
        'pyqt5-tools',
        'qimage2ndarray',
    ],
    url='',
    license='',
    author='Lukas Pertel',
    author_email='lukas.pertel@ttu.ee',
    description='Generic cellular automata simulator project for bachelor\'s thesis.'
)
