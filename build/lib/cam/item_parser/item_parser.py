from cam.parsers.parser import Parser


class ItemParser(Parser):
    def __init__(self, add_help=True, parents=[]):
        super().__init__(add_help=add_help, parents=parents, fromfile_prefix_chars="@")
        self.description = "CAM Renderer item parser"
        self.add_argument("--primary-state", dest="primary_state", type=int,
                          help="Set state/color for renderer drawing mode")
        self.add_argument("--set-pixels", dest="set_pixels", type=float, nargs="*",
                          help="Change state of pixels, input is a list of (row, col)")


if __name__ == "__main__":
    parser = ItemParser()
    print(parser.parse_known_args("--set-pixels 1 2 3 4 5 6 7.0 9.9".split()))
