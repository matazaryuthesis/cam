import argparse

import sys


class Parser(argparse.ArgumentParser):
    def error(self, message):
        print(message, file=sys.stderr)
        # print("Input is not valid. '-h' or '--help' for help")  # , file=sys.stderr)
        self.print_usage(file=sys.stderr)
        # raise TypeError

    # def exit(self, status=0, message=None):
    #     print("PARSER EXIT CAUGHT")

    def catch_help_argument(self, args, print_help=True):
        if "--help" in args or "-h" in args:
            args = [arg for arg in args if arg not in ["-h", "--help"]]
            if print_help:
                print("Help argument was caught, help will be shown:")
                self.print_help()
        return args
