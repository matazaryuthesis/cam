from abc import ABCMeta, abstractmethod


class Rule(metaclass=ABCMeta):
    def __init__(self, name):
        self.name = name

    @abstractmethod
    def rule(self, lattice):
        raise NotImplementedError
