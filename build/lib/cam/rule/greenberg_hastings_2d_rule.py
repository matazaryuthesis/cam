import numpy as np

from cam.lattice.lattice_2d_factory import Lattice2DFactory
from cam.rule.rule_base import Rule


class GreenbergHastingsRule(Rule):
    def __init__(self):
        super().__init__("2D:Greenberg-Hastings")

    def rule(self, lattice):
        # Simulates torus topology by padding the lattice with states from other sides
        resting_state, excited_state, refractory_state = 0, 1, 2
        pad_lattice = np.pad(lattice, 1, mode="wrap")
        neighbours = (pad_lattice[0:-2, 1:-1] == excited_state) | (pad_lattice[1:-1, 0:-2] == excited_state) | (
                pad_lattice[1:-1, 2:] == excited_state) | (pad_lattice[2:, 1:-1] == excited_state)
        own_state = pad_lattice[1:-1, 1:-1]
        # resting = ((own_state == refractory_state) | ((own_state == resting_state) & (neighbours == 0)))
        excited = (own_state == resting_state) & (neighbours)
        refractory = (own_state == excited_state)

        # lattice[resting] = resting_state
        lattice[:] = resting_state
        lattice[excited] = excited_state
        lattice[refractory] = refractory_state
        return lattice


if __name__ == "__main__":
    # lattice = np.array([[0, 0, 0, 0], [0, 1, 2, 0], [0, 0, 0, 0], [0, 0, 0, 0]])
    lattice = Lattice2DFactory.create_random_lattice(10, 10, 2)
    # print("Initial:", lattice, sep="\n")
    print("Initial:", lattice.get_lattice(), sep="\n")
    rule = GreenbergHastingsRule()
    lattice.update(rule)
    print("Updated:", lattice.get_lattice(), sep="\n")
    # print("Updated:", rule.rule(lattice), sep="\n")
    # print("Updated:", rule.rule(lattice), sep="\n")
    # print("Updated:", rule.rule(lattice), sep="\n")
