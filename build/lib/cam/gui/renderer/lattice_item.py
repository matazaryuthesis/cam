import argparse
import time

from PyQt5 import Qt, QtCore

from cam.parsers.item_parser import ItemParser


class LatticeItem(Qt.QGraphicsPixmapItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.test = False
        # self.setShapeMode(QtWidgets.QGraphicsPixmapItem.BoundingRectShape)
        self.primary_state_index = kwargs.get("primary_state_index", 0)
        self.secondary_state_index = None  # TODO mousePressEvent and mouseReleaseEvent implemenation.
        self.color_table = [Qt.qRgb(i * 50 % 256, i * 133 % 256, i * 70 % 256) for i in range(256)]
        self.new_modifications = False

    def mousePressEvent(self, event):
        event.accept()
        if self.test: print(event.pos(), "LatticeItem mousePressEvent")
        if self.timer is not None:
            self.timer_was_active = self.timer.isActive()
            if self.timer.isActive():
                self.timer.stop()
        print("Bounds:", self.boundingRect())
        self.set_pixel(event.pos(), self.primary_state_index)

    def mouseMoveEvent(self, event):
        event.accept()
        if self.test: print(event.pos(), "LatticeItem mouseMoveEvent")
        self.set_pixel(event.pos(), self.primary_state_index)

    def mouseReleaseEvent(self, event):
        event.accept()
        if self.timer is not None and self.timer_was_active:
            self.timer.start()
        if self.test: print(event.pos(), "LatticeItem mouseReleaseEvent")

    def mouseDoubleClickEvent(self, event):
        event.accept()
        if self.test: print(event.pos(), "LatticeItem mouseDoubleClickEvent")

    def set_pixel(self, qpointf, index=None):
        start = time.perf_counter()
        if not self.contains(qpointf):
            print("Trying to set pixel out of bounds.")
        if index is None:
            # print("No active pixel to set")
            # return
            index = self.primary_state_index
        x = int(qpointf.x())
        y = int(qpointf.y())
        print("Set pixel | x:", x, "y:", y, "i:", index)
        img = self.pixmap().toImage()
        img = img.convertToFormat(Qt.QImage.Format_Indexed8)
        if self.color_table is not None:
            img.setColorTable(self.color_table)
        else:
            print("No color table?")
        if self.test: print("Pixel index before:", img.pixelIndex(x, y))
        img.setPixel(x, y, index)
        if self.test: print("Pixel index after:", img.pixelIndex(x, y))
        self.setPixmap(Qt.QPixmap.fromImage(img))
        self.update()
        self.new_modifications = True
        if self.test: print("Pixel set, time:", time.perf_counter() - start)

    def set_primary_state(self, index):
        self.primary_state_index = index

    def set_secondary_state(self, index):
        self.secondary_state_index = index

    def set_color_table(self, color_table):
        self.color_table = color_table

    def setPixmap(self, pixmap):
        super().setPixmap(pixmap)
        self.new_modifications = False

    def set_timer(self, timer):
        self.timer = timer

    def parse_arguments(self, args):
        parser = ItemParser()
        namespace, unknown_args = argparse.Namespace(), dict()
        try:
            namespace, unknown_args = parser.parse_known_args(args)
        except BaseException:
            pass
        self.handle_options(vars(namespace))

    def handle_options(self, options):
        if options.get("primary_state") is not None:
            self.set_primary_state(options.get("primary_state"))
        if options.get("set_pixels") is not None:
            n = options.get("set_pixels")
            point = QtCore.QPointF
            for i in range(1, len(n), 2):
                self.set_pixel(point(n[i - 1], n[i]))
