import argparse

from PyQt5 import QtWidgets, Qt

from cam.parsers.view_parser import ViewParser


class LatticeView(QtWidgets.QGraphicsView):
    ZOOM_IN_MULTIPLIER = 1.25
    ZOOM_OUT_MULTIPLIER = 0.8

    def __init__(self, *args, **kwargs):
        print("Args:", args, "Kwargs:", kwargs)
        super().__init__(*args, **kwargs)
        self.test = False
        self.init_ui()
        self.create_actions()

    def init_ui(self):
        self.setBackgroundBrush(Qt.QBrush(Qt.QColor.fromRgb(50, 50, 50, 255)))
        self.scroll_hand_drag_mode()
        self.setAlignment(Qt.Qt.AlignCenter)

    def create_actions(self):
        print("Create actions")
        self.zoom_in_action = QtWidgets.QAction("Zoom &In (25%)", self, shortcut="+",
                                                enabled=True, triggered=self.zoom_in)

        self.zoom_out_action = QtWidgets.QAction("Zoom &Out (25%)", self, shortcut="-",
                                                 enabled=True, triggered=self.zoom_out)

        self.zoom_reset_action = QtWidgets.QAction("Zoom Reset", self, shortcut="r",
                                                   enabled=True, triggered=self.zoom_reset)

        self.addActions([self.zoom_in_action, self.zoom_out_action, self.zoom_reset_action])
        print("Actions created")

    def toggle_drag_mode(self):
        if self.dragMode() == Qt.QGraphicsView.ScrollHandDrag:
            self.no_drag_mode()
        else:
            self.scroll_hand_drag_mode()

    def no_drag_mode(self):
        self.setDragMode(Qt.QGraphicsView.NoDrag)
        self.setInteractive(True)

    def scroll_hand_drag_mode(self):
        self.setDragMode(Qt.QGraphicsView.ScrollHandDrag)
        self.setInteractive(False)

    def keyPressEvent(self, event):
        if event.key() == Qt.Qt.Key_Control:
            self.no_drag_mode()
        else:
            event.ignore()
            return

    def keyReleaseEvent(self, event):
        if event.key() == Qt.Qt.Key_Control:
            self.scroll_hand_drag_mode()
        else:
            event.ignore()

    def wheelEvent(self, event):
        oldPos = self.mapToScene(event.pos())
        # Zoom
        # self.centerOn(oldPos)
        if self.test: print("oldPos:", oldPos)
        if self.test: print("Event.pos()", event.pos(), "Event.angleDelta()", event.angleDelta(),
                            event.angleDelta().y(),
                            event.angleDelta() / 8)
        if event.angleDelta().y() > 0:
            if self.test: print("zoomIn")
            self.zoom_in()
        else:
            if self.test: print("zoomOut")
            self.zoom_out()
        if self.test: print("newPos:", self.mapToScene(event.pos()))

    def zoom_in(self):
        self.zoom(self.ZOOM_IN_MULTIPLIER)

    def zoom_out(self):
        self.zoom(self.ZOOM_OUT_MULTIPLIER)

    def zoom(self, multiplier):
        self.scale(multiplier, multiplier)

    def zoom_scale(self, new_zoom_scale):
        self.zoom_reset()
        self.zoom(new_zoom_scale)

    def zoom_reset(self):
        self.resetTransform()

    def parse_arguments(self, args):
        parser = ViewParser()
        namespace, unknown_args = argparse.Namespace(), dict()
        try:
            namespace, unknown_args = parser.parse_known_args(args)
        except BaseException:
            pass
        self.handle_view_arguments(vars(namespace))

    def handle_view_arguments(self, options):
        if options.get("zoom_in"):
            self.zoom_in()
        elif options.get("zoom_out"):
            self.zoom_out()
        elif options.get("zoom_reset"):
            self.zoom_reset()
        elif options.get("zoom_scale") is not None:
            self.zoom_scale(options.get("zoom_scale"))
