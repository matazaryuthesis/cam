import sys
import time

import numpy as np
import qimage2ndarray
from PyQt5 import QtGui, QtCore, QtWidgets, Qt

from cam.gui.renderer.lattice_item import LatticeItem
from cam.gui.renderer.lattice_view import LatticeView
from cam.lattice.lattice_1d_factory import Lattice1DFactory
from cam.lattice.lattice_2d_factory import Lattice2DFactory
from cam.lattice.lattice_factory import LatticeFactory
from cam.parsers.old_parsers import LatticeTypeParser, Lattice1DParser, Lattice2DParser
from cam.rule.conway_life_2d_rule import ConwayGameOfLifeRule
from cam.rule.elementary_1d_rule import ElementaryRule
from cam.rule.greenberg_hastings_2d_rule import GreenbergHastingsRule


class Example(QtWidgets.QWidget):
    def __init__(self, args):
        super().__init__()
        # self.lattice.get_data() = np.require(self.lattice.get_data(), np.uint8, 'C')
        self.test = False
        if self.test: print("Init example")
        self.init_lattice(args)

        self.setup()
        self.init_ui()
        # self.createActions()
        self.display()
        # self.timer.start()
        self.resize(512, 512)
        if self.test: print("End of init")

    def parse_commands(self, args):
        if args.__class__ is not list:
            raise ValueError("Parse_commands(args) argument should be a list not a", args.__class_)
        if self.test: print("Parse commands:", args)


        lattice_type_parser = LatticeTypeParser()
        lattice_1d_parser = Lattice1DParser()
        lattice_2d_parser = Lattice2DParser()

        args = self.catch_help_argument(args)

        type_ns, unknown_args = lattice_type_parser.parse_known_args(args)
        dimensions = type_ns.dimensions
        lattice_factory = LatticeFactory
        if dimensions == 1:
            self.one_dimensional = True
            ns, unknown_args = lattice_1d_parser.parse_known_args(unknown_args)
            if self.test: print("Lattice1D namespace:", ns)
            # self.lattice = Lattice1DFactory.create_lattice(ns.data, ns.columns, ns.states)
            self.lattice = lattice_factory.create_lattice(dimensions, lattice=ns.data, columns=ns.columns,
                                                          states=ns.states)

            self.rule = ElementaryRule(ns.elementary_rule)
            self.lattice.set_rule(self.rule)
        elif dimensions == 2:
            self.one_dimensional = False
            ns, unknown_args = lattice_2d_parser.parse_known_args(unknown_args)
            if self.test: print("Lattice21D namespace:", ns)
            self.lattice = Lattice2DFactory.create_lattice(ns.data, ns.size, ns.states)
            self.rule = ConwayGameOfLifeRule() if ns.rule == "gol" else GreenbergHastingsRule() if ns.rule == "gh" else None
            self.lattice.set_rule(self.rule)
        else:
            raise ValueError

        print("Unknown arguments:", unknown_args, file=sys.stderr)

    def catch_help_argument(self, args_list):
        if "--help" in args_list or "-h" in args_list:
            print("Help argument was caught, help will be shown:")
            args_list[:] = [arg for arg in args_list if arg != "-h" and arg != "--help"]
            LatticeTypeParser().print_help()
            Lattice1DParser().print_help()
            Lattice2DParser().print_help()
            # sys.exit()
        return args_list

    def handle_lattice_1d_parser(self, ns):
        self.lattice = Lattice1DFactory.create_lattice(ns.data, ns.columns, ns.states)
        rules = {"empty": Lattice1DFactory.create_empty_lattice, }

    def setup(self):
        self.lattice_item = LatticeItem()
        self.lattice_item.setFlags(Qt.QGraphicsItem.ItemClipsToShape)

        # self.color_table = [QtGui.qRgb(255, 255, 255), QtGui.qRgb(0, 0, 0), QtGui.qRgb(200, 125, 125)]
        self.color_table = [Qt.qRgb(i * 50 % 256, i * 133 % 256, i * 70 % 256) for i in range(256)]
        print("Color table:", self.color_table)
        self.lattice_item.set_color_table(self.color_table)

        self.active_index = 1
        self.lattice_item.set_primary_state(self.active_index)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.lattice_item.set_timer(self.timer)

        self.scene = Qt.QGraphicsScene()
        self.scene.addItem(self.lattice_item)

    def init_lattice(self, args):
        self.scaleFactor = 1.0
        self.pixmap = None
        self.cols = self.rows = 512
        self.update_count = 0

        self.parse_commands(args)

        # if one_dimensional:
        #     # self.lattice = Lattice1DNumpy(self.cols, test_set=True)
        #     self.lattice = Lattice1DFactory.create_center_active_lattice(self.cols)
        #     self.rule = ElementaryRule(30)
        #     self.lattice.set_rule(self.rule)
        # else:
        #     # self.lattice = Lattice2DNumpyRuleForLattice(self.rows, self.cols, test_set=False)
        #     self.lattice = Lattice2DFactory.create_random_lattice(self.rows, self.cols, 2)
        #     # self.rule = GameOfLifeRule()
        #     self.rule = GreenbergHastingsRule()
        #     # self.lattice = Lattice2DFactory.create_test_lattice()
        #     self.lattice.set_rule(self.rule)

    def update(self):
        if self.test: print("Update")
        # print("updating")
        self.front_to_back()
        start_update = time.perf_counter()
        if self.test: print("GUI update lattice")
        self.lattice.update()
        print("Lattice update:", time.perf_counter() - start_update)
        start_display = time.perf_counter()
        self.display()
        print("Display update:", time.perf_counter() - start_display)
        # self.scroll_area.verticalScrollBar().setValue(self.scroll_area.verticalScrollBar().maximum())

        # print("changing title")
        self.update_count += 1
        self.setWindowTitle(str(self.update_count))

    def front_to_back(self):
        if self.test: print("front_to_back()")
        if not self.lattice_item.new_modifications:
            if self.test: print("front_to_back(): no modifications")
            return
        print("New modifications")
        img = self.lattice_item.pixmap().toImage()
        # print("OOOPS")
        img = img.convertToFormat(Qt.QImage.Format_Indexed8)
        if self.color_table is not None:
            img.setColorTable(self.color_table)
        cells_array = qimage2ndarray.raw_view(img)
        self.lattice.set_lattice(cells_array)

    def display(self):
        if self.test: print("test")
        array = np.require(self.lattice.get_lattice(), np.uint8, 'C')
        img = qimage2ndarray.gray2qimage(array)
        img.setColorTable(self.color_table)
        if self.test: print("test2")
        self.lattice_item.setPixmap(Qt.QPixmap.fromImage(img))
        if self.one_dimensional == True:
            self.vertical_slider_to_maximum()
        if self.test: print("test3")

    def vertical_slider_to_maximum(self):
        view_vsb = self.lattice_view.verticalScrollBar()
        print(view_vsb.maximum(), view_vsb.sliderPosition(), view_vsb.pageStep(), view_vsb.singleStep())
        if self.one_dimensional == True:
            view_vsb.setMaximum(view_vsb.maximum() + view_vsb.singleStep())
        # view_vsb.setSliderPosition(view_vsb.maximum() + view_vsb.singleStep())
        # print(view_vsb.maximum(), view_vsb.sliderPosition(), view_vsb.pageStep(), view_vsb.singleStep())
        view_vsb.triggerAction(Qt.QAbstractSlider.SliderToMaximum)
        print(view_vsb.maximum(), view_vsb.sliderPosition(), view_vsb.pageStep(), view_vsb.singleStep(),
              view_vsb.value())

    def toggle_update(self):
        # print("toggle_update start")
        print(self.timer.isActive())
        if self.timer.isActive():
            self.stop_update()
        else:

            self.timer.start()
            print("Started")

    def stop_update(self):
        if self.timer.isActive():
            self.timer.stop()
            print("Stopped")

    def update_once(self):
        self.stop_update()
        Qt.QTimer.singleShot(0, self.update)

    def keyPressEvent(self, event):
        if type(event) == QtGui.QKeyEvent:
            # here accept the event and do something
            print("Key pressed:", event.key())
            if event.key() == Qt.Qt.Key_Space:
                self.toggle_update()
            elif event.key() == Qt.Qt.Key_N:
                self.update_once()
            elif event.key() == Qt.Qt.Key_0:
                self.active_index = 0
                self.lattice_item.set_primary_state(self.active_index)
                print("Active index:", self.active_index)
            elif event.key() == Qt.Qt.Key_1:
                self.active_index = 1
                self.lattice_item.set_primary_state(self.active_index)
                print("Active index:", self.active_index)
            elif event.key() == Qt.Qt.Key_2:
                self.active_index = 2
                self.lattice_item.set_primary_state(self.active_index)
                print("Active index:", self.active_index)
            elif event.key() == Qt.Qt.Key_S:
                pix = self.lattice_item.pixmap()
                img = pix.toImage()
                self.lattice_item.pixmap().save("test drawing pix.png", "PNG")
            else:
                event.ignore()
        else:
            event.ignore()

    def init_ui(self):
        print("initUI")
        self.layout = QtWidgets.QVBoxLayout(self)

        self.lattice_view = LatticeView(self.scene)
        self.line_edit = Qt.QLineEdit()
        self.line_edit.returnPressed.connect(self.give_commands)
        self.layout.addWidget(self.lattice_view)
        self.layout.addWidget(self.line_edit)
        self.setLayout(self.layout)
        self.setWindowTitle(str(self.update_count))
        self.show()

    def give_commands(self):
        args = self.line_edit.text().split()
        print("New commands:", args)
        self.parse_commands(args)
        self.line_edit.setText("")
        self.display()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    args = sys.argv
    # print("Args:", args)
    ex = Example(args)

    sys.exit(app.exec_())
