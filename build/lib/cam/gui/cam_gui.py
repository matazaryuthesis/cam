import argparse
import sys
import time

import numpy as np
import qimage2ndarray
from PyQt5 import Qt, QtWidgets, QtCore, QtGui

from cam.cellular_automata_machine import CellularAutomataMachine
from cam.gui.command_line import CommandLine
from cam.gui.renderer.lattice_renderer import LatticeRenderer
from cam.parsers.update_parser import UpdateParser


class CAMGui(QtWidgets.QWidget):
    def __init__(self, args):
        self.app = QtWidgets.QApplication(args)
        super().__init__()
        self.test = True
        args = self.app.arguments()
        self.cam = CellularAutomataMachine(args)

        self._init_ui()
        self._init_functionality()
        self._init_lattice_renderer()

        self.show()
        sys.exit(self.app.exec())

    def _init_ui(self):
        self._lattice_renderer = LatticeRenderer()
        self._command_line = CommandLine(self)

        self._layout = QtWidgets.QVBoxLayout()
        self._layout.addWidget(self._lattice_renderer)
        self._layout.addWidget(self._command_line)
        self.setLayout(self._layout)

        self.setWindowTitle("CAM")

    def _init_functionality(self):
        # self._command_input_line.lineEdit().returnPressed.connect(self._give_commands)

        self.color_table = [Qt.qRgb(i * 50 % 256, i * 133 % 256, i * 70 % 256) for i in range(256)]

    def _init_lattice_renderer(self):
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)  # TODO update
        self._lattice_renderer._item.set_timer(self.timer)
        self._lattice_updated = True
        # self.timer.start()

    def _init_lattice(self):
        # TODO
        raise NotImplementedError

    def update(self):
        if self.test: print("GUI update()")
        # print("updating")
        self._renderer_to_cam()

        start_update = time.perf_counter()
        if self.test: print("GUI update lattice")
        self.cam.lattice.update()
        print("Lattice update:", time.perf_counter() - start_update)
        start_display = time.perf_counter()
        self.display()
        print("Display update:", time.perf_counter() - start_display)
        # self.scroll_area.verticalScrollBar().setValue(self.scroll_area.verticalScrollBar().maximum())

        # print("changing title")
        self.setWindowTitle(str(self.cam.lattice.step))

    def _renderer_to_cam(self):
        if self.test: print("front_to_back()")
        lattice_item = self._lattice_renderer._item
        if not lattice_item.new_modifications:
            if self.test: print("front_to_back(): no modifications")
            return
        print("New modifications")
        img = lattice_item.pixmap().toImage()
        # print("OOOPS")
        img = img.convertToFormat(Qt.QImage.Format_Indexed8)
        print("Renderer to CAM:", img.colorTable())
        if self.color_table is not None:
            img.setColorTable(self.color_table)
        cells_array = qimage2ndarray.raw_view(img)
        self.cam.lattice.set_lattice(cells_array)
        # self._lattice_renderer._item.new_modifications = False

    def display(self):
        if self.test: print("display()")
        array = np.require(self.cam.lattice.get_lattice(), np.uint8, 'C')
        img = qimage2ndarray.gray2qimage(array)
        img.setColorTable(self.color_table)
        if self.test: print("test2")
        self._lattice_renderer._item.setPixmap(QtGui.QPixmap.fromImage(img))
        if self.cam.lattice_options.get("dimensions") == 1:
            self.vertical_slider_to_maximum()
        qrectf = self._lattice_renderer._item.boundingRect()
        qrectf.setHeight(qrectf.height() + 2)
        self._lattice_renderer._view.setSceneRect(qrectf)
        # print("Scene size:", self._lattice_renderer._scene.sceneRect())
        # print("View size:", self._lattice_renderer._view.sceneRect())
        # print("Item size:", self._lattice_renderer._item.boundingRect())

        if self.test: print("test3")

    def vertical_slider_to_maximum(self):
        view_vsb = self._lattice_renderer._view.verticalScrollBar()
        # print("Slider info A:", view_vsb.maximum(), view_vsb.sliderPosition(), view_vsb.pageStep(),
        #       view_vsb.singleStep())
        view_vsb.triggerAction(QtWidgets.QAbstractSlider.SliderToMaximum)
        # print("Slider info B:", view_vsb.maximum(), view_vsb.sliderPosition(), view_vsb.pageStep(),
        #       view_vsb.singleStep(),
        #       view_vsb.value())

    def toggle_update(self):
        # print("toggle_update start")
        print(self.timer.isActive())
        if self.timer.isActive():
            self.stop_update()
        else:
            self.timer.start()
            print("Started")

    def stop_update(self):
        if self.timer.isActive():
            self.timer.stop()
            print("Stopped")
            return True
        return False

    def update_once(self):
        self.stop_update()
        Qt.QTimer.singleShot(0, self.update)

    def parse_arguments(self, args):
        leftover_args = self.cam.parse_arguments(args)
        update_parser = UpdateParser()
        namespace, unknown_args = argparse.Namespace(), dict()
        try:
            namespace, unknown_args = update_parser.parse_known_args(args)
        except BaseException:
            pass
        self.handle_update_arguments(vars(namespace))
        self._lattice_renderer._view.parse_arguments(args)
        self._lattice_renderer._item.parse_arguments(args)

    def handle_update_arguments(self, options):
        if options.get("pause"):
            self.stop_update()
        elif options.get("unpause"):
            self.timer.start()
        elif options.get("toggle"):
            self.toggle_update()
        elif options.get("step"):
            self.update_once()

    def keyPressEvent(self, event):
        if type(event) == QtGui.QKeyEvent:
            # here accept the event and do something
            print("Key pressed:", event.key())
            if event.key() == Qt.Qt.Key_Space:
                self.toggle_update()
            elif event.key() == Qt.Qt.Key_N:
                self.update_once()
            elif event.key() == Qt.Qt.Key_0:
                self.active_index = 0
                self._lattice_renderer._item.set_primary_state(self.active_index)
                print("Active index:", self.active_index)
            elif event.key() == Qt.Qt.Key_1:
                self.active_index = 1
                self._lattice_renderer._item.set_primary_state(self.active_index)
                print("Active index:", self.active_index)
            elif event.key() == Qt.Qt.Key_2:
                self.active_index = 2
                self._lattice_renderer._item.set_primary_state(self.active_index)
                print("Active index:", self.active_index)
            elif event.key() == Qt.Qt.Key_S:
                pix = self._lattice_renderer._item.pixmap()
                img = pix.toImage()
                self._lattice_renderer._item.pixmap().save("test drawing pix.png", "PNG")
            else:
                event.ignore()
        else:
            event.ignore()


if __name__ == "__main__":
    app = CAMGui(sys.argv)
