import numpy as np

from cam.lattice.lattice_base import Lattice


class Lattice1D(Lattice):
    def __init__(self, lattice):
        print("Initializing 1D lattice")
        super().__init__(lattice)

    def _update_inner(self, rule):
        last_row = self._lattice[-1]  # In 1D we only update last row
        data_copy = np.copy(last_row)
        updated_data = rule(data_copy)
        return np.concatenate((self._lattice, updated_data.reshape(1, *updated_data.shape)))

    def _set_cell(self, location, state):
        self._lattice[-1, location] = state
