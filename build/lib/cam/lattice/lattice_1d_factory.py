import numpy as np

from cam.lattice.lattice_1d import Lattice1D


class Lattice1DFactory(object):
    @staticmethod
    def create_random_lattice(columns, states):
        print("Lattice1DFactory.create_random_lattice():", columns, states)
        data = np.random.randint(0, states, (1, columns), dtype=np.uint8)
        return Lattice1D(data)

    @classmethod
    def create_empty_lattice(cls, columns):
        print("Lattice1DFactory.create_empty_lattice():", columns)
        return cls.create_random_lattice(columns, 1)

    @classmethod
    def create_center_active_lattice(cls, columns):
        print("Lattice1DFactory.create_center_active_lattice():", columns)
        lattice = cls.create_empty_lattice(columns)
        middle_cell = int(columns / 2)
        lattice.set_cell(middle_cell, 1)
        return lattice

    @classmethod
    def create_lattice(cls, lattice, columns=512, states=2):
        print("Lattice1DFactory.create_lattice():", lattice, columns, states)
        if lattice == "empty":
            return cls.create_empty_lattice(columns)
        elif lattice == "random":
            return cls.create_random_lattice(columns, states)
        elif lattice == "center":
            return cls.create_center_active_lattice(columns)
        else:
            raise ValueError("Invalid type for 1D lattice: {}.".format(lattice))
