from cam.parsers.parser import Parser


class GuiParser(Parser):
    def __init__(self, add_help=False, parents=[]):
        super().__init__(add_help=add_help, parents=parents)
        self.description = "A simple cellular automata machine engine parser."


class NewParser(Parser):
    def __init__(self, add_help=False, parents=[]):
        super().__init__(add_help=add_help, parents=parents, fromfile_prefix_chars="@")
        self.description = "CAM test parser."
        lattice_subparsers = self.add_subparsers(help="sub-command help")
        parser_1d = lattice_subparsers.add_parser("1d", help="1d help")
        parser_1d.add_argument("--columns", type=int, nargs=1, dest="size", default=[512],
                               help="column size (type:%(type)s) (default:%(default)s)")
        parser_1d.add_argument("--rule")

        # self.add_argument("-q", "--quit", action="store_true", help="will exit the program if set")
        self.add_argument("-l", "--lattice", choices=["1d", "2d"])
        self.add_argument("-ur", "--update_rule", nargs=2,
                          help="First argument code filename, second argument function name'")
        self.add_argument("-i", "--initial_config", help="Filename to the initial configuration of lattice")
        self.add_argument("--custom_lattice", nargs=2)
        self.add_argument("-r", "--rows", type=int, help="number of rows in lattice")
        self.add_argument("-c", "--columns", type=int, help="number of columns in lattice")


class LatticeTypeParser(Parser):
    def __init__(self, add_help=True, parents=[]):
        super().__init__(add_help=add_help, parents=parents, fromfile_prefix_chars="@")
        self.description = "CAM Lattice type parser"
        self.add_argument("-d", "--dimensions", type=int, choices=[1, 2], default=1,
                          help="type of lattice (choices: %(choices)s) (default:%(default)s)")


class Lattice1DParser(Parser):
    def __init__(self, add_help=True, parents=[]):
        super().__init__(add_help=add_help, parents=parents, fromfile_prefix_chars="@")
        self.description = "1D lattice options parser"
        self.add_argument("-c", "--columns", type=int, nargs=1, default=512,
                          help="Size [columns] of 1D lattice (default:%(default)s)")
        self.add_argument("--states", type=int, choices=[i for i in range(1, 257)], default=2, metavar="{1...257}",
                          help="Different states used for 'random' lattice, (choices: %(metavar)s) (default:%(default)s)")
        self.add_argument("-l", "--lattice", type=str.lower, choices=["empty", "random", "center"], default="center",
                          dest="data",
                          help="Initial lattice configuration (choices: %(choices)s) (default:%(default)s)")
        self.add_argument("-r", "--rule", type=int, choices=[i for i in range(256)], default=30, dest="elementary_rule",
                          metavar="{0...255}",
                          help="1D elementary rule number (choices: %(metavar)s) (default:%(default)s)")


class Lattice2DParser(Parser):
    def __init__(self, add_help=True, parents=[]):
        super().__init__(add_help=add_help, parents=parents, fromfile_prefix_chars="@")
        self.description = "2D lattice options parser"
        self.add_argument("-s", "--size", type=int, nargs=2, default=[512, 512],
                          help="Size [rows, columns] of 2D lattice (default:%(default)s)")
        self.add_argument("--states", type=int, choices=[i for i in range(1, 257)], default=2, metavar="{1...257}",
                          help="Different states used for 'random' lattice, (choices: %(metavar)s) (default:%(default)s)")
        self.add_argument("-l", "--lattice", type=str.lower, choices=["empty", "random", "glider"], default="random",
                          dest="data",
                          help="Initial lattice configuration (choices: %(choices)s) (default:%(default)s)")
        self.add_argument("-r", "--rule", type=str.lower, choices=["gol", "gh"], default="gol",
                          help="Built in rule to use on the lattice (choices: %(choices)s) (default:%(default)s)")
