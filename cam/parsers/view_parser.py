from cam.parsers.parser import Parser


class ViewParser(Parser):
    def __init__(self, add_help=True, parents=[]):
        super().__init__(add_help=add_help, parents=parents, fromfile_prefix_chars="@")
        self.description = "CAM Renderer parser"
        zooming_group = self.add_mutually_exclusive_group()
        zooming_group.add_argument("--zoom-in", dest="zoom_in", action="store_true",
                                   help="Zoom in with lattice renderer")
        zooming_group.add_argument("--zoom-out", dest="zoom_out", action="store_true",
                                   help="Zoom out with lattice renderer")
        zooming_group.add_argument("--zoom-reset", dest="zoom_reset", action="store_true",
                                   help="Reset lattice renderer zoom")
        zooming_group.add_argument("--zoom-scale", dest="zoom_scale", type=float,
                                   help="Zoom to specific scale")


if __name__ == "__main__":
    parser = ViewParser()
    print(parser.parse_known_args("--zoom-in --zoom-o".split()))
