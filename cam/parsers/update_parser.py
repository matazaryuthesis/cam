from cam.parsers.parser import Parser


class UpdateParser(Parser):
    def __init__(self, add_help=True, parents=[]):
        super().__init__(add_help=add_help, parents=parents, fromfile_prefix_chars="@")
        self.description = "CAM Update parser"
        update_group = self.add_mutually_exclusive_group()
        update_group.add_argument("-p", "--pause", dest="pause", action="store_true",
                                  help="Sets the simulation state to paused/stopped if it is running")
        update_group.add_argument("-u", "--unpause", dest="unpause", action="store_true",
                                  help="Sets the simulation state to unpaused/running")
        update_group.add_argument("-t", "--toggle", dest="toggle", action="store_true",
                                  help="Reverses the simulation state (Stopped <-> Running)")
        update_group.add_argument("-s", "--step", dest="step", action="store_true",
                                  help="Simulates a single update and then pauses")
