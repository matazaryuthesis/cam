from cam.parsers.parser import Parser


class ItemParser(Parser):
    def __init__(self, add_help=True, parents=[]):
        super().__init__(add_help=add_help, parents=parents, fromfile_prefix_chars="@")
        self.description = "CAM Renderer item parser"
        self.add_argument("--primary-state", dest="primary_state", type=int,
                          help="Set state/color for renderer drawing mode")
        self.add_argument("--set-cells", dest="set_cells", type=int, nargs="*",
                          help="Change state of cells, input is a list of (row, col). \
                          If list length is odd, then last element will be discarded. \
                          If used with a 1D lattice, row values will be ignored.")


if __name__ == "__main__":
    parser = ItemParser()
    print(parser.parse_known_args("--set-cells 1 2 3 4 5 6 7.0 9.9".split()))
