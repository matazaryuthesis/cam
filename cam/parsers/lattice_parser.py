from cam.parsers.parser import Parser


class LatticeParser(Parser):
    def __init__(self, add_help=True, parents=[]):
        super().__init__(add_help=add_help, parents=parents, fromfile_prefix_chars="@")
        # if defaults is None:
        #     defaults = dict()
        # self.set_defaults(**defaults)
        self.description = "CAM Lattice parser"
        self.add_argument("-d", "--dimensions", type=int, choices=[1, 2],
                          help="Lattice dimensions (choices: %(choices)s) (default:%(default)s)")
        self.add_argument("-c", "--cols", dest="columns", type=int,
                          help="Column size of 1D & 2D lattices (default:%(default)s)")
        self.add_argument("-r", "--rows", dest="rows", type=int,
                          help="Row size of 2D lattice. Will be ignored for 1D lattices (always 1 row). (default:%(default)s)")
        # group_2d.add_argument("-s", "--shape", dest="shape", type=int, nargs=2,
        #                                    help="Shape [rows, columns] of lattice. Only columns will be used in case of 1D lattice. (default:%(default)s)")
        self.add_argument("--states", dest="states", type=int, choices=[i for i in range(1, 257)],
                          metavar="{1...257}",
                          help="Different states used for 'random' lattice, (choices: %(metavar)s) (default:%(default)s)")
        self.add_argument("--1d-lattice", dest="lattice_1d", type=str.lower, choices=["empty", "random", "center"],
                          help="1D lattice configuration (choices: %(choices)s) (default:%(default)s)")
        self.add_argument("--2d-lattice", dest="lattice_2d", type=str.lower, choices=["empty", "random", "glider"],
                          help="2D lattice configuration (choices: %(choices)s) (default:%(default)s)")
        self.add_argument("--1d-rule", dest="rule_1d", type=int, choices=[i for i in range(256)],
                          metavar="{0...255}",
                          help="1D elementary rule number (choices: %(metavar)s) (default:%(default)s)")
        self.add_argument("--2d-rule", dest="rule_2d", type=str.lower, choices=["gol", "gh"],
                          help="Built in rule to use on the lattice (choices: %(choices)s) (default:%(default)s)")


if __name__ == "__main__":
    starting_lattice_options = {"dimensions": 1,
                                "columns": 512,
                                "rows": 512,
                                "shape": [1, 512],
                                "states": 2,
                                "lattice_1d": "center",
                                "lattice_2d": "random",
                                "rule_1d": 30,
                                "rule_2d": "gol",
                                }
    # parser = LatticeParser(defaults=starting_lattice_options)
    parser = LatticeParser()
    parser.set_defaults(**starting_lattice_options)
