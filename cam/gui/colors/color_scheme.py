import numpy as np
from PyQt5 import QtGui


class ColorScheme(object):
    def __init__(self, color_list=None):
        if color_list is None:
            self.color_table = np.array([QtGui.qRgb(i * 50 % 256, i * 133 % 256, i * 70 % 256) for i in range(256)])
        else:
            self.color_table = color_list

    def set_state_color(self, state, color):
        if 0 <= state <= 255:
            self.color_table[state] = color
            return True
        return False

    def get_color_table(self):
        return self.color_table

    def set_color_table(self, new_color_list):
        if len(new_color_list) == 256 and new_color_list.dtype == np.uint8:
            self.color_table = new_color_list[:256]
            return True
        return False
