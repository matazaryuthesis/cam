import argparse
import sys
import time

import qimage2ndarray
from PyQt5 import QtWidgets, QtCore, QtGui

from cam.cellular_automata_machine import CellularAutomataMachine
from cam.gui.colors.color_scheme import ColorScheme
from cam.gui.command_line import CommandLine
from cam.gui.renderer.lattice_renderer import LatticeRenderer
from cam.parsers.update_parser import UpdateParser


class CAMGui(QtWidgets.QWidget):
    def __init__(self, args):
        self.app = QtWidgets.QApplication(args)
        super().__init__()

        self.init_elements()
        self.init_ui()

        self.show()
        sys.exit(self.app.exec())

    def init_elements(self):
        args = self.app.arguments()
        self.cam = CellularAutomataMachine(args)

        self.color_scheme = ColorScheme()
        self.command_line = CommandLine(self)

        self.init_lattice_renderer()

    def init_lattice_renderer(self):
        self.lattice_renderer = LatticeRenderer(gui=self)
        self.lattice_renderer.set_color_scheme(self.color_scheme)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)  # TODO update
        self.lattice_renderer._item.set_timer(self.timer)

    def init_ui(self):
        self.gui_layout = QtWidgets.QVBoxLayout()
        self.gui_layout.addWidget(self.lattice_renderer)
        self.gui_layout.addWidget(self.command_line)

        self.setLayout(self.gui_layout)
        self.setWindowTitle("Cellular Automata Machine")

    def update(self, update_lattice=True, update_display=True):
        print("CAMGui.update()")
        if update_lattice:
            print("GUI update lattice")
            start_update = time.perf_counter()
            self.cam.lattice.update()
            print("Lattice update:", time.perf_counter() - start_update)
        if update_display:
            start_display = time.perf_counter()
            self.display()
            print("Display update:", time.perf_counter() - start_display)

        self.setWindowTitle("CAM " + str(self.cam.lattice.step))  # TODO Should be displayed somewhere else

    def display(self):
        print("CAMGui.display()")
        lattice_data = self.cam.lattice.get_lattice()
        img = qimage2ndarray.gray2qimage(lattice_data)
        img.setColorTable(self.color_scheme.get_color_table())

        lattice_pixmap = QtGui.QPixmap.fromImage(img)
        self.lattice_renderer._item.setPixmap(lattice_pixmap)

        self.recalculate_renderer_view_size()
        if self.cam.lattice_options.get("dimensions") == 1:
            self.vertical_slider_to_maximum()

    def recalculate_renderer_view_size(self):
        qrectf = self.lattice_renderer._item.boundingRect()
        qrectf.setHeight(qrectf.height() + 2)
        self.lattice_renderer._view.setSceneRect(qrectf)

    def vertical_slider_to_maximum(self):
        view_vsb = self.lattice_renderer._view.verticalScrollBar()
        view_vsb.triggerAction(QtWidgets.QAbstractSlider.SliderToMaximum)

    def toggle_update(self):
        if self.timer.isActive():
            print("Toggle update: Stop")
            self.stop_update()
        else:
            print("Toggle update: Start")
            self.timer.start()
            print("Started")

    def stop_update(self):
        if self.timer.isActive():
            self.timer.stop()
            print("Update timer: Stopped")
            return True
        print("Update timer: Can't stop, not running")
        return False

    def update_once(self):
        self.stop_update()
        QtCore.QTimer.singleShot(0, self.update)

    def parse_arguments(self, args):
        leftover_args = self.cam.parse_arguments(args)
        update_parser = UpdateParser()
        namespace, unknown_args = argparse.Namespace(), dict()
        try:
            namespace, unknown_args = update_parser.parse_known_args(args)
        except BaseException:
            pass
        self.handle_update_arguments(vars(namespace))
        self.lattice_renderer._view.parse_arguments(args)
        self.lattice_renderer._item.parse_arguments(args)

    def handle_update_arguments(self, options):
        if options.get("pause"):
            self.stop_update()
        elif options.get("unpause"):
            self.timer.start()
        elif options.get("toggle"):
            self.toggle_update()
        elif options.get("step"):
            self.update_once()

    def keyPressEvent(self, event):
        if type(event) == QtGui.QKeyEvent:
            # here accept the event and do something
            print("Key pressed:", event.key())
            if event.key() == QtCore.Qt.Key_Space:
                self.toggle_update()
            elif event.key() == QtCore.Qt.Key_N:
                self.update_once()
            elif event.key() == QtCore.Qt.Key_0:
                self.active_index = 0
                self.lattice_renderer._item.set_primary_state(self.active_index)
                print("Active index:", self.active_index)
            elif event.key() == QtCore.Qt.Key_1:
                self.active_index = 1
                self.lattice_renderer._item.set_primary_state(self.active_index)
                print("Active index:", self.active_index)
            elif event.key() == QtCore.Qt.Key_2:
                self.active_index = 2
                self.lattice_renderer._item.set_primary_state(self.active_index)
                print("Active index:", self.active_index)
            elif event.key() == QtCore.Qt.Key_S:
                pix = self.lattice_renderer._item.pixmap()
                img = pix.toImage()
                self.lattice_renderer._item.pixmap().save("test drawing pix.png", "PNG")
            else:
                event.ignore()
        else:
            event.ignore()


if __name__ == "__main__":
    app = CAMGui(sys.argv)
