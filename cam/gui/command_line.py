from PyQt5 import QtWidgets


class CommandLine(QtWidgets.QComboBox):
    def __init__(self, gui, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.gui = gui
        self.setEditable(True)
        self._init_default_commands()
        self.lineEdit().returnPressed.connect(self._give_commands)

    def _init_default_commands(self):
        commands = ["",
                    "--primary-state 1",
                    "-d 1 --1d-rule 111 --1d-lattice center",
                    "-d 1 --1d-rule 30 --1d-l random -c 300",
                    "--1d-l random",
                    "-d 2",
                    "-d 2 --2d-r gol --2d-l glider --rows 512 -c 512",
                    "-d 2 --2d-r gh --2d-lat glider --rows 1000",
                    "-d 2 --2d-r gh --2d-l random",
                    "--primary-state 1 --set-cells 50 50 100 100 101 101"]
        # for command in commands:
        #     qcommand = QtCore.QString(command)
        #     print(qcommand)
        #     self.insertItem(0, qcommand)
        self.insertItems(0, commands)

    def _give_commands(self):
        print("Command line")
        # args = self._command_input_line.text().split()
        args = self.currentText().split()
        print("New commands:", args)
        self.gui.parse_arguments(args)
        # self.clearEditText()
        # self._lattice_updated = True
        self.gui.update(update_lattice=False)
