import argparse

from PyQt5 import Qt, QtCore

from cam.parsers.item_parser import ItemParser


class LatticeItem(Qt.QGraphicsPixmapItem):
    def __init__(self, *args, gui, color_scheme=None, **kwargs):
        super().__init__(*args, **kwargs)
        if gui is None:
            raise ValueError
        self.gui = gui
        # self.setShapeMode(QtWidgets.QGraphicsPixmapItem.BoundingRectShape)
        self.primary_state_index = kwargs.get("primary_state_index", 0)
        self.secondary_state_index = kwargs.get("secondary_state_index",
                                                0)  # TODO mousePressEvent and mouseReleaseEvent implemenation.
        self.new_modifications = False

        self.color_scheme = color_scheme
        print("LatticeItem: Init color scheme:", self.color_scheme)


    def mousePressEvent(self, event):
        print(event.pos(), "LatticeItem.mousePressEvent(): Start")
        event.accept()
        self.timer_was_active = self.gui.timer.isActive()
        if self.gui.timer.isActive():
            self.gui.timer.stop()
        print("Bounds:", self.boundingRect())
        self.set_cell(event.pos(), self.primary_state_index)
        print(event.pos(), "LatticeItem.mousePressEvent(): End")

    def mouseMoveEvent(self, event):
        print(event.pos(), "LatticeItem.mouseMoveEvent()")
        event.accept()
        self.set_cell(event.pos(), self.primary_state_index)

    def mouseReleaseEvent(self, event):
        print(event.pos(), "LatticeItem.mouseReleaseEvent()")
        event.accept()
        if self.timer_was_active:
            self.gui.timer.start()

    def mouseDoubleClickEvent(self, event):
        event.accept()
        print(event.pos(), "LatticeItem mouseDoubleClickEvent")

    def set_cell(self, qpointf, state=None):
        print("LatticeItem.set_cell()")
        if not self.contains(qpointf):
            print("Trying to set pixel out of bounds.")
            return False
        if state is None:
            state = self.primary_state_index
        x = int(qpointf.x())
        y = int(qpointf.y())
        cell_coordinates = (y, x)
        # print("LatticeItem.setPixel(): Set cell (y,x):", cell_coordinates, "state:", state)
        # img = self.pixmap().toImage()
        # img = img.convertToFormat(Qt.QImage.Format_Indexed8)
        # print("LatticeItem.set_cell(): colorScheme -", self.color_scheme.get_color_table())
        # print("LatticeItem.set_cell(): imgFormat -", img.format())
        # print("LatticeItem.set_cell(): imgColorTable -", img.colorTable())
        # print("LatticeItem.set_cell(): imgColorTable -", img.colorCount())

        # print("LatticeItem.setPixel(): update image pixel")
        # img.setPixel(x, y, state)

        self.gui.cam.lattice.set_cell(cell_coordinates, state)
        # print("LatticeItem.setPixel(): change to updated image")
        # self.setPixmap(Qt.QPixmap.fromImage(img))
        self.new_modifications = True
        self.gui.display()
        return True

    def set_primary_state(self, index):
        self.primary_state_index = index

    def set_secondary_state(self, index):
        self.secondary_state_index = index

    def set_color_scheme(self, color_scheme):
        self.color_scheme = color_scheme

    def setPixmap(self, pixmap):
        super().setPixmap(pixmap)
        self.new_modifications = False

    def set_timer(self, timer):
        self.timer = timer

    def parse_arguments(self, args):
        parser = ItemParser()
        namespace, unknown_args = argparse.Namespace(), dict()
        try:
            namespace, unknown_args = parser.parse_known_args(args)
        except BaseException:
            pass
        self.handle_options(vars(namespace))

    def handle_options(self, options):
        if options.get("primary_state") is not None:
            self.set_primary_state(options.get("primary_state"))
        if options.get("set_cells") is not None:
            # if self.timer is not None:
            #     self.timer_was_active = self.timer.isActive()
            #     if self.timer.isActive():
            #         self.timer.stop()

            n = options.get("set_cells")
            point = QtCore.QPointF
            for i in range(1, len(n), 2):
                self.set_cell(point(n[i - 1], n[i]))
                # self.set_cell((n[i - 1], n[i]), self.primary_state_index)
            # if self.timer is not None and self.timer_was_active:
            #     self.timer.start()
