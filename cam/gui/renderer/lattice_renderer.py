from PyQt5 import QtWidgets

from cam.gui.renderer.lattice_item import LatticeItem
from cam.gui.renderer.lattice_view import LatticeView


class LatticeRenderer(QtWidgets.QWidget):
    def __init__(self, gui, color_scheme=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if gui is None:
            raise ValueError("LatticeRenderer: GUI must be set on initialization.")
        self._gui = gui
        print("LatticeRenderer parent", self.parent.__name__)
        self._color_scheme = color_scheme
        self._init_elements()
        self._init_ui()

    def _init_elements(self):
        self._item = LatticeItem(gui=self._gui, color_scheme=self._color_scheme)
        # http://doc.qt.io/qt-5/qgraphicsitem.html#GraphicsItemFlag-enum
        self._item.setFlags(QtWidgets.QGraphicsItem.ItemClipsToShape)

        self._scene = QtWidgets.QGraphicsScene()
        self._view = LatticeView(self._scene, gui=self._gui)
        self._scene.addItem(self._item)
        # self._lattice_item.set_p

    def _init_ui(self):
        self._layout = QtWidgets.QVBoxLayout()
        self._layout.addWidget(self._view)
        self.setLayout(self._layout)

    def set_color_scheme(self, new_color_scheme):
        self._color_scheme = new_color_scheme
        self._item.set_color_scheme(self._color_scheme)

    def set_lattice(self, new_lattice):
        # TODO
        raise NotImplementedError
