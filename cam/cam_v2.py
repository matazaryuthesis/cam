import ast
import sys

from cam.lattice.lattice1d import Lattice1D
from cam.lattice.lattice2d import Lattice2D
from cam.parsers.cam_parsers import EngineParser, Lattice1DParser, Lattice2DParser, ProgramParser

from cam.cam_engine import CAMEngine


class CAM(object):
    def __init__(self, **kwargs):
        self.program_parser = kwargs.get("program_parser", ProgramParser())
        self.engine_parser = kwargs.get("engine_parser", EngineParser())
        self.lattice_1d_class = kwargs.get("lattice_1d_class", Lattice1D)
        if not issubclass(self.lattice_1d_class, Lattice1D):
            raise TypeError("Given 1D lattice class is not a subclass of lattice.lattice1d.Lattice1D")
        self.lattice_1d_parser = kwargs.get("lattice_1d_parser",
                                            Lattice1DParser([self.engine_parser, self.program_parser]))
        self.lattice_2d_class = kwargs.get("lattice_2d_class", Lattice2D)
        if not issubclass(self.lattice_2d_class, Lattice2D):
            raise TypeError("Given 2D lattice class is not a subclass of lattice.lattice2d.Lattice2D")
        self.lattice_2d_parser = kwargs.get("lattice_2d_parser",
                                            Lattice2DParser([self.program_parser, self.engine_parser]))
        self.engine = kwargs.get("engine", CAMEngine(self.lattice_1d_class()))
        if not issubclass(self.lattice_1d_class, Lattice1D):
            raise TypeError("Given engine class is not a subclass of cam.cam_engine.CAMEngine")
        self.lattice_type = kwargs.get("lattice_type", "1d")
        self.set_lattice_and_parser_and_handler(self.lattice_type)
        self.exit = False

    def input_loop(self):
        try:
            while not self.exit:
                try:
                    self.parse_input(input())
                except KeyboardInterrupt:
                    self.exit = True
        finally:
            self.terminate()

    def parse_input(self, user_input):
        user_input_split = user_input.split()
        user_input_split = self.catch_help_argument(user_input_split)
        try:
            ns, extra = self.main_parser.parse_known_args(user_input_split)
            self.handle_input(ns)
            self.print_unknown_args_if_exists(extra)
        except Exception as e:
            print("Error while parsing:", e, file=sys.stderr)

    def handle_input(self, ns):
        try:
            self.lattice_handler(ns)
            if self.program_handler(ns):
                return
            self.engine_handler(ns)
        except Exception as e:
            print("Error while handling:", e, file=sys.stderr)

    def handle_lattice_1d_options(self, ns):
        self.engine.lattice.set_columns(ns.columns)
        self.engine.lattice.lattice_to_state(ns.lattice_to_state)
        self.handle_1d_set_cells(ns.set_cells)

    def handle_lattice_2d_options(self, ns):
        self.engine.lattice.set_rows_columns(ns.rows, ns.columns)
        if ns.lattice_to_state is not None:
            self.engine.lattice.lattice_to_state(ns.lattice_to_state)
        self.handle_2d_set_cells(ns.set_cells)

    def handle_1d_set_cells(self, set_cellsList):
        if set_cells is None:
            return
        set_cells_split = [set_cells[i:i + 2] for i in range(0, len(set_cells), 2)]
        for index in range(len(set_cells_split)):
            cell_data = set_cells_split[index]
            if len(cell_data) == 2:
                column = cell_data[0]
                state = cell_data[1]
                self.engine.lattice.set_cell(column, state)

    def handle_2d_set_cells(self, set_cells):
        if set_cells is None:
            return
        set_cells_split = [set_cells[i:i + 3] for i in range(0, len(set_cells), 3)]
        for index in range(len(set_cells_split)):
            cell_data = set_cells_split[index]
            if len(cell_data) == 3:
                row = cell_data[0]
                column = cell_data[1]
                state = cell_data[2]
                self.engine.lattice.set_cell(row, column, state)

    def engine_handler(self, ns):
        if ns.status:
            self.status()
        if ns.steps is not None:
            self.engine.set_steps(ns.steps)
        if ns.display is not None:
            self.engine.set_display_nth_step(ns.display)
        if ns.show:
            self.engine.lattice.display()
        if ns.pause:
            self.engine.pause()
        if ns.unpause:
            self.engine.unpause()

    def program_handler(self, ns):
        if ns.quit:
            self.exit = True
            raise SystemExit
        if ns.initial_config:
            self.handle_initial_configuration(ns.initial_config)
        if ns.update_rule:
            self.handle_update_rule(ns.update_rule)
        if ns.custom_lattice:
            self.handle_custom_lattice(ns.custom_lattice)
        return self.set_lattice_and_parser_and_handler(ns.lattice)

    def handle_initial_configuration(self, filename):
        # print(filename)
        if filename is None:
            return
        with open(filename) as file:
            lattice = [list(map(int, line.split())) for line in file]
        # print(lattice)
        self.engine.lattice.set_lattice(lattice)

    def handle_update_rule(self, data):
        print(data)
        if data is None:
            return
        with open(data[0]) as file:
            update_rule_str = file.read()

        if not self.is_valid_python(update_rule_str):
            print("Not valid python code")
            return

        compiled = compile(update_rule_str, data[0], "exec")
        update_rule_ns = {}
        exec(compiled, update_rule_ns)

        self.engine.lattice.set_update_rule(update_rule_ns[data[1]])

    def handle_custom_lattice(self, data):
        print(data)
        if data is None:
            return
        with open(data[0]) as file:
            custom_lattice_str = file.read()
        if not self.is_valid_python(custom_lattice_str):
            print("Not valid python code")
            return
        print(custom_lattice_str)
        compiled = compile(custom_lattice_str, data[0], "exec")
        custom_lattice_ns = {}
        exec(compiled, custom_lattice_ns)
        new_lattice = custom_lattice_ns[data[1]]()
        print("Lattice2D class: {}, New lattice class: {}".format(Lattice2D, new_lattice.__class__))
        print(new_lattice.lattice)
        if isinstance(new_lattice, Lattice1D):
            self.set_lattice_and_parser_and_handler("1d")
        elif isinstance(new_lattice, Lattice2D):
            self.set_lattice_and_parser_and_handler("2d")
        else:
            print("New lattice isn't an instance of Lattice1D nor Lattice2D!", file=sys.stderr)
            return
        self.engine.set_lattice(new_lattice)

    def print_unknown_args_if_exists(self, unknown_args=[]):
        if len(unknown_args) > 0:
            print("Unknown arguments:", unknown_args)

    def set_lattice_and_parser_and_handler(self, dimensions=None):
        if dimensions == "1d":
            self.engine.set_lattice(self.lattice_1d_class())
            self.main_parser = self.lattice_1d_parser
            self.lattice_handler = self.handle_lattice_1d_options
            return True
        elif dimensions == "2d":
            self.engine.set_lattice(self.lattice_2d_class())
            self.main_parser = self.lattice_2d_parser
            self.lattice_handler = self.handle_lattice_2d_options
            return True

    def catch_help_argument(self, args_list):
        if "--help" in args_list or "-h" in args_list:
            print("Help argument was caught, help will be shown:")
            args_list[:] = [arg for arg in args_list if arg != "-h" and arg != "--help"]
            self.main_parser.print_help()
        return args_list

    def status(self):
        self.engine.status()

    def start(self):
        self.engine.start()
        self.input_loop()

    def terminate(self):
        self.exit = True
        self.engine.terminate()
        print("PROGRAM EXITED")

    @staticmethod
    def is_valid_python(code):
        try:
            ast.parse(code)
        except SyntaxError as se:
            print(str(se))
            return False

        return True


if __name__ == "__main__":
    # kwargs = {
    # "engine": CAMEngine(),
    # "engine_parser": EngineParser(),
    # "lattice_1d_class": Lattice1D,
    # "lattice_1d_parser": Lattice1DParser(),
    # "lattice_2d_class": Lattice2D,
    # "lattice_2d_parser": Lattice2DParser()
    # }
    cam = CAM()
    CAM().start()
