from abc import ABCMeta, abstractmethod

import numpy as np


class Lattice(metaclass=ABCMeta):
    def __init__(self, lattice, rule=None):
        print("Initializing base lattice")
        self._lattice = lattice
        self.step = 0
        self.rule = rule

    def update(self, update_rule=None):
        if update_rule is None and self.rule is None:
            raise ValueError("Lattice needs a rule!")
        elif update_rule is None:
            update_rule = self.rule

        self._lattice = self._update_inner(update_rule.rule)
        self.step += 1
        return True

    def set_rule(self, rule):
        self.rule = rule

    def get_rule(self, rule):
        return self.rule

    @abstractmethod
    def _update_inner(self, rule):
        raise NotImplementedError

    def get_lattice(self):
        return np.copy(self._lattice)

    def set_lattice(self, new_lattice):
        self._lattice = new_lattice
        self.step = 0

    def set_cell(self, location, state):
        self._set_cell(location, state)
        self.step = 0

    @abstractmethod
    def _set_cell(self, location, state):
        raise NotImplementedError
