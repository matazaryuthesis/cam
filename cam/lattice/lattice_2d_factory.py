import numpy as np

from cam.lattice.lattice_2d import Lattice2D


class Lattice2DFactory(object):
    @staticmethod
    def create_random_lattice(rows, columns, states):
        data = np.random.randint(0, states, (rows, columns), dtype=np.uint8)

        return Lattice2D(data)

    @classmethod
    def create_empty_lattice(cls, rows, columns):
        return cls.create_random_lattice(rows, columns, 1)

    @classmethod
    def create_glider_lattice(cls, rows, columns):
        pattern = Pattern2DFactory.create_glider()
        y, x = pattern.shape
        rows = max(rows, y)
        columns = max(columns, x)

        lattice = cls.create_empty_lattice(rows, columns)
        data = lattice.get_lattice()
        data[0:y, 0:x] = pattern
        lattice.set_lattice(data)
        return lattice

    @classmethod
    def create_lattice(cls, lattice, size=(512, 512), states=2):
        if lattice == "empty":
            return cls.create_empty_lattice(*size)
        elif lattice == "random":
            return cls.create_random_lattice(*size, states)
        elif lattice == "glider":
            return cls.create_glider_lattice(*size)
        else:
            raise ValueError("Invalid type for 2D lattice: {}.".format(lattice))


class Pattern2DFactory(object):
    @classmethod
    def create_pattern(cls, pattern_name):
        return {"glider": cls.create_glider(),
                "lwss": cls.create_lwss(),
                }.get(pattern_name)

    @classmethod
    def create_glider(cls):
        pattern = """
        010
        001
        111
        """
        return cls._string_to_array(pattern)

    @classmethod
    def create_lwss(cls):
        pattern = """
        10010
        00001
        10001
        01111
        """
        return cls._string_to_array(pattern)

    @classmethod
    def create_beacon(cls):
        pattern = """
        1100
        1101
        0011
        """
        return cls._string_to_array(pattern)

    @staticmethod
    def _string_to_array(pattern, whitespace=False):
        no_whitespace_mapping = lambda row: list(map(int, row.strip()))
        whitespace_mapping = lambda row: list(map(int, row.split()))
        active_mapping = whitespace_mapping if whitespace == True else no_whitespace_mapping
        lattice = [active_mapping(row) for row in pattern.strip().split("\n")]
        lattice = np.array(lattice, dtype=np.uint8)
        return np.pad(lattice, 1, mode="constant")  # Pad around with zeros


if __name__ == "__main__":
    print(Lattice2DFactory.create_glider_lattice().get_lattice())
