from cam.lattice.lattice_1d_factory import Lattice1DFactory
from cam.lattice.lattice_2d_factory import Lattice2DFactory


class LatticeFactory(object):
    @staticmethod
    def create_lattice(dimensions, *args, **kwargs):
        if dimensions == 1:
            factory = Lattice1DFactory
        elif dimensions == 2:
            factory = Lattice2DFactory
        else:
            raise ValueError
        return factory.create_lattice(*args, **kwargs)
