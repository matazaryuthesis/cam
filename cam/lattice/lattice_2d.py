import numpy as np

from cam.lattice.lattice_base import Lattice


class Lattice2D(Lattice):
    def __init__(self, lattice):
        super().__init__(lattice)

    def _update_inner(self, rule):
        return rule(np.copy(self._lattice))

    def _set_cell(self, location, state):
        self._lattice[location] = state
