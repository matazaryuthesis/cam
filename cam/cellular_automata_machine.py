import argparse
import sys

from cam.lattice.lattice_1d_factory import Lattice1DFactory
from cam.lattice.lattice_2d_factory import Lattice2DFactory
from cam.lattice.lattice_factory import LatticeFactory
from cam.parsers.lattice_parser import LatticeParser
from cam.rule.rule_1d_factory import Rule1DFactory
from cam.rule.rule_2d_factory import Rule2DFactory


class CellularAutomataMachine(object):
    starting_lattice_options = {"dimensions": 1,
                                "columns": 512,
                                "rows": 512,
                                "shape": [1, 512],
                                "states": 2,
                                "lattice_1d": "center",
                                "lattice_2d": "random",
                                "rule_1d": 30,
                                "rule_2d": "gol",
                                }

    def __init__(self, args=None):
        self.initialize_defaults()
        self.initialize_lattice()
        if args is not None:
            self.parse_arguments(args)

    def initialize_defaults(self):
        self.lattice_options = dict(CellularAutomataMachine.starting_lattice_options)
        self.parser = LatticeParser()  # TODO dependency injection
        # self.parser.set_defaults(**self.lattice_options)
        self.lattice_factory = LatticeFactory  # TODO dependency injection
        self.lattice_1d_factory = Lattice1DFactory  # TODO dependency injection
        self.rule_1d_factory = Rule1DFactory  # TODO dependency injection
        self.lattice_2d_factory = Lattice2DFactory  # TODO dependency injection
        self.rule_2d_factory = Rule2DFactory  # TODO dependency injection

    def initialize_lattice(self):
        self.handle_arguments(self.lattice_options)

    def handle_arguments(self, new_options):
        new_dimensions = new_options.get("dimensions")
        new_rows = new_options.get("rows")
        new_columns = new_options.get("columns")
        new_lattice_1d = new_options.get("lattice_1d")
        new_lattice_2d = new_options.get("lattice_2d")
        new_rule_1d = new_options.get("rule_1d")
        new_rule_2d = new_options.get("rule_2d")

        changed_options = {k: v for k, v in new_options.items() if v is not None}
        print("Changed options:", changed_options, file=sys.stderr)
        self.lattice_options.update(changed_options)

        dimensions = self.lattice_options.get("dimensions")
        rows = self.lattice_options.get("rows")
        columns = self.lattice_options.get("columns")
        lattice_1d = self.lattice_options.get("lattice_1d")
        lattice_2d = self.lattice_options.get("lattice_2d")
        rule_1d = self.lattice_options.get("rule_1d")
        rule_2d = self.lattice_options.get("rule_2d")
        states = self.lattice_options.get("states")
        print("Lattice options:", self.lattice_options)

        if new_dimensions is not None:
            print("Lattice dimension was changed")
            if new_dimensions == 1:
                print("Creating new 1d lattice")
                self.lattice = self.lattice_1d_factory.create_lattice(lattice_1d, columns, states)
                self.lattice.set_rule(self.rule_1d_factory.create_rule(rule_1d))
            elif new_dimensions == 2:
                print("Creating new 2d lattice")
                self.lattice = self.lattice_2d_factory.create_lattice(lattice_2d, (rows, columns), states)
                self.lattice.set_rule(self.rule_2d_factory.create_rule(rule_2d))
            else:
                raise ValueError
        elif dimensions == 1:
            if new_lattice_1d is not None or new_columns is not None:
                print("Updating 1d lattice data")
                self.lattice = self.lattice_1d_factory.create_lattice(lattice_1d, columns, states)
                # self.lattice.set_rule(self.rule_1d_factory.create_rule(rule_1d))
            self.lattice.set_rule(self.rule_1d_factory.create_rule(rule_1d))
        elif dimensions == 2:
            if new_lattice_2d is not None or new_columns is not None or new_rows is not None:
                print("Updating 2d lattice data")
                self.lattice = self.lattice_2d_factory.create_lattice(lattice_2d, (rows, columns), states)
            self.lattice.set_rule(self.rule_2d_factory.create_rule(rule_2d))
        print("Dimensions:", dimensions, "Lattice shape:", self.lattice._lattice.shape, "Lattice rule:",
              self.lattice.rule.name)

    def parse_arguments(self, args):
        # self.parser.set_defaults(**self.lattice_options)
        args = self.parser.catch_help_argument(args)
        print("Arguments:", args, file=sys.stderr)
        namespace, unknown_args = argparse.Namespace(), dict()
        try:
            namespace, unknown_args = self.parser.parse_known_args(args)
        except (BaseException) as e:
            print("BaseException (SystemExit) was caught during argument parsing (invalid values)")

        print("Unknown arguments:", unknown_args, file=sys.stderr)
        new_options = vars(namespace)
        print("New options:", new_options, file=sys.stderr)
        self.handle_arguments(new_options)
        return unknown_args


if __name__ == "__main__":
    cam = CellularAutomataMachine()
