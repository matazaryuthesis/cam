import numpy as np

from cam.rule.rule_base import Rule


class ElementaryRule(Rule):
    def __init__(self, rule_number):
        number = rule_number % 256
        name = "1D:Elementary{}".format(number)
        super().__init__(name)
        self._filter = self._create_filter(number)

    def _create_filter(self, number):
        binary_8bit = format(number, '08b')
        mask = np.array(list(map(int, binary_8bit)))
        combinations = np.array([111, 110, 101, 100, 11, 10, 1, 0])
        filter = combinations[mask == 1]
        return filter

    def rule(self, lattice):
        padded_lattice = np.pad(lattice, 1, mode="wrap")  # In order to simulate torus topology
        neighbours = padded_lattice[0:-2] * 100 + padded_lattice[1:-1] * 10 + padded_lattice[2:]  # Neighbourhoods
        active = np.in1d(neighbours, self._filter)  # Active/live cells
        updated = np.where(active, 1, 0)
        return updated


if __name__ == "__main__":
    el_rule = ElementaryRule(30)
    lattice = np.array([1, 1, 1, 0, 1, 1, 1, 0, 1, 1])
    print("Rule:", el_rule.name, "filter:", el_rule._filter)
    print(el_rule.rule(lattice))
