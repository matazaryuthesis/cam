import numpy as np

from cam.rule.rule_base import Rule


class ConwayGameOfLifeRule(Rule):
    def __init__(self):
        super().__init__("2D:Game of Life")

    def rule(self, lattice):
        # Simulates torus topology by padding the lattice with states from other sides
        pad_lattice = np.pad(lattice, 1, mode="wrap")
        neighbours = (pad_lattice[0:-2, 0:-2] + pad_lattice[0:-2, 1:-1] + pad_lattice[0:-2, 2:] +
                      pad_lattice[1:-1, 0:-2] + pad_lattice[1:-1, 2:] +
                      pad_lattice[2:, 0:-2] + pad_lattice[2:, 1:-1] + pad_lattice[2:, 2:])
        # own_state = pad_lattice[1:-1, 1:-1]
        birth = (neighbours == 3) & (pad_lattice[1:-1, 1:-1] == 0)
        survive = ((neighbours == 2) | (neighbours == 3)) & (pad_lattice[1:-1, 1:-1] == 1)
        return np.where(birth | survive, 1, 0)


if __name__ == "__main__":
    lattice = np.random.randint(0, 2, 25).reshape(5, 5)
    print(lattice)
    rule = ConwayGameOfLifeRule()
    print(rule.rule(lattice))
