from cam.rule.elementary_1d_rule import ElementaryRule


class Rule1DFactory(object):
    @classmethod
    def create_rule(cls, rule_number):
        if isinstance(rule_number, int) and 0 <= rule_number <= 255:
            return ElementaryRule(rule_number)
        else:
            raise ValueError
