from cam.rule.conway_life_2d_rule import ConwayGameOfLifeRule
from cam.rule.greenberg_hastings_2d_rule import GreenbergHastingsRule


class Rule2DFactory(object):
    @classmethod
    def create_rule(cls, rule_name):
        if rule_name == "gol":
            return ConwayGameOfLifeRule()
        elif rule_name == "gh":
            return GreenbergHastingsRule()
        else:
            raise ValueError
