from threading import Thread, Event, Lock


class CAMEngine(Thread):
    def __init__(self, lattice=None):
        self.exit_event = Event()
        self.pause_event = Event()
        self.pause_event.set()
        self.steps_left = 0
        self.steps_lock = Lock()
        self.display_nth_step = 1
        Thread.__init__(self, target=self.main_loop, daemon=False, name="CAMEngine")
        self.lattice = lattice

    def status(self):
        status = "paused" if self.pause_event.is_set() else "unpaused"
        print("ENGINE - Status: {}, Steps left: {}, Display: {}-th".format(status, self.steps_left,
                                                                           self.display_nth_step))
        print(self.lattice.status())

    def main_loop(self):
        try:
            while (not self.exit_event.is_set()):
                while (not self.pause_event.is_set()):
                    if not self.steps_left == 0:
                        self.lattice.update()
                        if self.lattice.current_step % self.display_nth_step == 0:
                            self.lattice.display()
                        with self.steps_lock:
                            self.steps_left -= 1
                    else:
                        self.pause()
                        # self.pause_event.wait(0.0001)
                        # self.exit_event.wait(1)
        finally:
            print(self.getName() + " done running.")
            self.terminate()

    def terminate(self):
        self.pause_event.set()
        self.exit_event.set()

    def pause(self):
        # if not self.pause_event.is_set() and self.steps_left != 0:
        #     self.lattice.display()
        self.pause_event.set()

    def unpause(self):
        self.pause_event.clear()

    def set_lattice(self, lattice):
        if (isinstance(lattice, Lattice)):
            self.lattice = lattice

    def set_steps(self, steps=None):
        if steps is None:
            return

        with self.steps_lock:
            self.steps_left = steps

    def set_display_nth_step(self, every_nth=None):
        if every_nth is None:
            return

        self.display_nth_step = every_nth

    def is_paused(self):
        return self.pause_event.is_set()

    def toggle_pause(self):
        self.unpause() if self.is_paused() else self.pause()
